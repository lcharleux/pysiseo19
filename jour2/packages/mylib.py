import numpy as np

a = 5

def superfunc(x):
    """
    A function that is super !
    
    Args:
        * x: a number
        
    Returns:
        x + 1
        
    >>> mylib.superfunc(5)
    6    
    """
    return x+1